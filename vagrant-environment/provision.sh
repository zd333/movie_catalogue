#!/bin/bash
CONF_DIR=//vagrant/vagrant-environment/conf
QUIET_APT=-qq

echo "Installing packages ..."
sudo apt-get update --yes $QUIET_APT
sudo apt-get install --reinstall  -y --force-yes -o Dpkg::options::=--force-confold $QUIET_APT nginx git nodejs-legacy npm


echo "Installing Bower and dependencies ..."
npm install -g bower
cd /vagrant/desktop_browser_app/_bower
bower install --allow-root


echo "Copying Nginx configs ..."
sudo cp $CONF_DIR/nginx_site_configs/* /etc/nginx/sites-available/
sudo cp $CONF_DIR/nginx_config/* /etc/nginx/
sudo service nginx restart
