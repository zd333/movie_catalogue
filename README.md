##Resources
 * Repository https://bitbucket.org/zd333/movie_catalogue
 * App address in Vagrant box http://localhost:8050/
 * Temporary deploy (please note, IMDB server doesn't allow to use images in that way and returns 403 error, so with deployed app images will be broken) http://jockey-samantha-25466.netlify.com/#/

##Used tools
 * AngularJS
 * Twitter Bootstrap

##Some comments and mentions
 * One of app shortcomings is that pending HTTP requests are not being aborted when new search is submitted (so when user quickly submits new search - then data from previous request can be mixed in). This can be resolved with resolving $http timeout promise. I haven't implemented this for simplification. Also, it is the reason why I use submit button instead of handling dynamically control values with ngChange directive :).
 * didn't use minified libraries' versions for simplification.
 
##About caсhing
The easiest way of cache implementation would be to store serialized response data using url (or serialized params object) as a key.
$http service has native support of such caching (setting 'chache' to true in http request would be enought).
###But this simple approach has several shortcomings:
 * data duplication (different search requests can intersect, so the same movies will be stored in different cache records)
 * cache of search request can't be used in movie details view

###That's why I decided to implement kind of smart caching:
 * single movie details record will be cached as is with 'imdbID' key
 * search request will be cached as array of single movie IDs

In real app this approach wouldn't work with omdbapi, because getting movie by ID returns more detailed data than search requests.
But task requirements say: "На странице можно отображать ту же информацию, что и в таблице.", so I assume my caching model is fine for test app.

