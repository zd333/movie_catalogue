;
(function() {
  'use strict';

  angular.module('movieCatalogue.routes', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
      $routeProvider
        .when('/', {
          controller: 'MovieSetController',
          controllerAs: 'msc',
          templateUrl: 'partials/movie_set.html',
          reloadOnSearch: false
        })
        .when('/movie_details/:imdbID', {
          controller: 'MovieDetailsController',
          controllerAs: 'mdc',
          templateUrl: 'partials/movie_details.html',
          reloadOnSearch: true
        })
        .otherwise({
          redirectTo: '/'
        });
    }]);
})();
