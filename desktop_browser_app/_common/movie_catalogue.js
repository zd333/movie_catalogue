;
(function() {
  'use strict';

  var movieCatalogue = angular.module('movieCatalogue', [
    'movieCatalogue.routes',
    'movieCatalogue.services',
    'movieCatalogue.controllers',
    'movieCatalogue.directives'
  ]);

  //service container
  angular.module('movieCatalogue.services', []);

  //controller container
  angular.module('movieCatalogue.controllers', []);

  //directive container
  angular.module('movieCatalogue.directives', []);

  movieCatalogue.run();
})();
