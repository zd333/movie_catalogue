;
(function() {
  'use strict';

  angular
    .module('movieCatalogue.services')
    .factory('BackendData', ['$http', '$q', '$cacheFactory', function($http, $q, $cacheFactory) {
      //prepare cache, it will be saved in closure
      //and shared between this service and all child services, since services are singletons
      var cache = $cacheFactory('movieHeap');

      return {
        get: get,
        loadFromBackEnd: loadFromBackEnd,
        pushToCache: pushToCache,
        getFromCache: getFromCache,
        emptyCache: emptyCache
      };

      function loadFromBackEnd(paramsObj) {
        //server must not bring CORS-related issues
        var DEFAULT_ERROR_MSG = 'Can not load data from server';
        var DOMAIN_PREFIX = 'http://www.omdbapi.com/';

        //build URL using properties of input object
        var url = '';
        for (var key in paramsObj) {
          url += '&' + key + '=' + paramsObj[key];
        }
        url = DOMAIN_PREFIX + url.replace('&', '?'); //change first char

        return $http.get(url)
          .then(function(successResponse) {
            //got back-end data, now check if it is not false response
            var d = successResponse.data;

            if (d) {
              if ((d.Response) && (d.Response.toLowerCase() === 'true')) {
                //ok, this appears to be useful data
                return d;
              } else {
                //perhaps, request with wrong parameter values
                return $q.reject(d.Error || DEFAULT_ERROR_MSG);
              }
            } else {
              //no data in response???? not sure how it can be, but handle anyway just in case
              return $q.reject(DEFAULT_ERROR_MSG);
            }
          }, function(errorResponse) {
            //error happened, return error message string
            return $q.reject(errorResponse.statusText || DEFAULT_ERROR_MSG);
          });
      }

      function get(paramsObj) {
        //try to get from cache first
        var buf = this.getFromCache(paramsObj);
        if (buf) {
          return $q.resolve(buf);
        } else {
          //was not found in cache - so get from backend
          var self = this;
          return this.loadFromBackEnd(paramsObj)
            .then(function(data) {
              self.pushToCache(paramsObj, data);
              return data;
            }, function(errorMsg) {
              return $q.reject(errorMsg);
            });
        }
      }

      function pushToCache(key, value) {
        if ((angular.isUndefined(key)) || (angular.isUndefined(value))) {
          return;
        }

        cache.put(JSON.stringify(key), value);
        console.log('Cached: ' + JSON.stringify(key));
      }

      function getFromCache(key) {
        var res = cache.get(JSON.stringify(key));
        if (res) console.log('Restored from cache: ' + JSON.stringify(key))
        return res;
      }

      function emptyCache() {
        cache.removeAll();
      }

    }]);
})();
