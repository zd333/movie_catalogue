;
(function() {
  'use strict';

  angular
    .module('movieCatalogue.services')
    .factory('Movie', ['BackendData', 'Models', '$q', function(BackendData, Models, $q) {
      var movie = Object.create(BackendData);
      //everything is inherited directly from BackendData services
      //only load method is overridden to process raw back-end data through model constructor

      //save original (parent) method from prototype to use them in overridden method
      var parentLoadFromBackEnd = movie.loadFromBackEnd;

      //override with own method
      movie.loadFromBackEnd = ownLoadFromBackEnd;

      return movie;

      function ownLoadFromBackEnd(paramsObj) {
        var deferred = $q.defer();

        //invoke parent method and then process raw data trough MovieModel constructor (to cut of unnecessary parameters, add default values, etc.)
        return parentLoadFromBackEnd(paramsObj)
          .then(function(data) {
            return new Models.MovieModel(data);
          }, function(errorMsg) {
            return $q.reject(errorMsg);
          });
      }

    }]);
})();
