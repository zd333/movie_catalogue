;
(function() {
  'use strict';

  angular
    .module('movieCatalogue.services')
    .factory('FilteredMovieSet', ['BackendData', 'Models', '$q', function(BackendData, Models, $q) {
      var movieSet = Object.create(BackendData);
      //override loadFromBackEnd, pushToCache and getFromCache methods
      //loadFromBackEnd method needs some customization, because search request returns not plain array of movies, but object with some additional data
      //cache methods need custom implementation with smart caching logic
      //the idea is to cache search results as a set of cached movies + additional cache record with IDs of those movies
      //this allows to use search cache in movie details view and helps avoid data duplication for intersected search requests (e.g. with search strings 'Finding Nemo' and 'Captain Nemo')

      //save original (parent) methods from prototype to use them in overridden methods
      var parentLoadFromBackEnd = movieSet.loadFromBackEnd;
      var parentPushToCache = movieSet.pushToCache;
      var parentGetFromCache = movieSet.getFromCache;

      //override with own methods
      movieSet.loadFromBackEnd = ownLoadFromBackEnd;
      movieSet.pushToCache = ownPushToCache;
      movieSet.getFromCache = ownGetFromCache;

      return movieSet;

      function ownLoadFromBackEnd(paramsObj) {
        //invoke parent method and then try to decompose results to array
        return parentLoadFromBackEnd(paramsObj)
          .then(function(data) {
            //check if result object contains 'Search' property and if it has the value of array type
            var a = data.Search;

            if ((a) && (a instanceof Array)) {
              a.forEach(function(elem, index, array) {
                array[index] = new Models.MovieModel(elem);
              });

              return a;
            } else {
              return $q.reject('Server API issue');
            }
          }, function(errorMsg) {
            return $q.reject(errorMsg);
          });
      }

      function ownPushToCache(key, value) {
        var movieIdsArray = [];
        var allElementsAreMovies = true;

        //verify if value is Array (it must be, since it is movie SET class)
        if (!value instanceof Array) {
          return;
        }

        value.forEach(function(elem) {
          if ((elem) && (elem.imdbID)) {
            movieIdsArray.push(elem.imdbID);

            parentPushToCache({
              i: elem.imdbID
            }, elem);
          } else {
            //this array element appears to be not a movie
            //it still makes sense to try caching other movies (to make them available for movie details view)
            allElementsAreMovies = false;
          }
        });

        if (allElementsAreMovies) {
          //caching of all movies from search set was fine, so can cache search request itself
          parentPushToCache(key, movieIdsArray);
        }
      }

      function ownGetFromCache(key) {
        //get movie set cache record
        var ids = parentGetFromCache(key);

        if (ids) {
          //restore all movies by IDs
          var movieSetFromCache = [];

          ids.forEach(function(elem) {
            var movie = parentGetFromCache({
              i: elem
            });

            if (!movie) {
              //at least one movie was not found in cache, so restoring search request can't be done
              return;
            }

            movieSetFromCache.push(movie);
          });

          return movieSetFromCache;
        }
      }

    }]);
})();
