;
(function() {
  'use strict';

  angular
    .module('movieCatalogue.services')
    .factory('Models', [function() {
      //container for model constructors
      return {
        MovieModel: MovieModel
      };

      function MovieModel(rawObject) {
        //simple constructor just to cut off unnecessary properties from raw JSON object (which is expected to come from backend)
        //data validation can be done here
        if (!rawObject) {
          //default object is empty
          //as alternative, default or random movie can be returned instead of empty object
          return {};
        }
        this.imdbID = rawObject.imdbID;
        this.Title = rawObject.Title;
        this.Year = rawObject.Year;
        this.Type = rawObject.Type;
        //replace empty poster with placeholder
        this.Poster = ((!rawObject.Poster) || (rawObject.Poster === 'N/A')) ? '/_common/img/placeholder.png' : rawObject.Poster;
      }

    }]);
})();
