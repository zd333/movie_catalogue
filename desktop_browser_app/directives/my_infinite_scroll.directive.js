(function() {
  'use strict';

  angular
    .module('movieCatalogue.directives')
    .directive('myInfiniteScroll', ['$window', function($window) {
      //infinite scroll
      //function dataAdderCallbackPromise must return promise!
      return {
        restrict: 'A',
        scope: {
          dataAdderCallbackPromise: '&myInfiniteScroll'
        },
        link: function(scope, el, attrs) {
          //this will store promise returned by data-adding outside function
          var promise;

          $window = angular.element($window);

          $window.on('scroll', scrollHandler);
          scope.$on('$destroy', function() {
            return $window.off('scroll', scrollHandler);
          });

          function scrollHandler() {
            if ((!promise) || (promise.$$state.status === 1)) {
              //make new requests only if no promise, or the promise is resolved (not pending or rejected)
              //this is to avoid multiply simultanious data requests while promise is pending
              //and to stop data adding when promise is rejected
              var wintop = window.pageYOffset;
              var docHeight = window.document.body.clientHeight;
              var windowHeight = window.innerHeight;
              var scroll = (wintop / (docHeight - windowHeight));

              if (scroll > 0.9) {
                promise = scope.$apply(scope.dataAdderCallbackPromise);
              }
            }
          }
        }
      };
    }]);
})();
