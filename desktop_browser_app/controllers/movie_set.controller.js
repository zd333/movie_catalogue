;
(function() {
  'use strict';

  angular
    .module('movieCatalogue.controllers')
    .controller('MovieSetController', ['FilteredMovieSet', '$timeout', function(FilteredMovieSet, $timeout) {
      var msc = this;

      msc.movies = [];
      msc.hasCache = false;
      msc.paginationDataAdder = paginationDataAdder;
      var curPage = 0;
      msc.submitSearch = submitSearch;
      msc.emptyCache = emptyCache;
      msc.errorMsg = '';

      msc.nameFilter = '';
      msc.yearFilter;
      msc.typeFilter = '';

      function submitSearch() {
        curPage = 0;
        msc.movies = [];
        //TODO: add a mechanism to abort all pending (not finished) requests here, to avoid mixing in data of previous submits
        if (!msc.nameFilter) {
          //back-end API doesn't allow search without title string
          return;
        }

        //load first portions of data, it can require requesting of several pages
        //this is to fill screen, make scroll appear and make infinite scroll directive request additional data
        //TODO: woud be goot to migrate this logic to directive
        (function bootLoad() {
          curPage++;

          FilteredMovieSet.get({
              s: msc.nameFilter,
              y: parseInt(msc.yearFilter) || '',
              type: msc.typeFilter,
              page: curPage
            })
            .then(function(movies) {
              msc.errorMsg = '';
              msc.hasCache = true;
              msc.movies = msc.movies.concat(movies);

              //check if scroll appeared
              //do it via timeout to allow DOM updates to be rendered
              $timeout(function() {
                var docHeight = window.document.body.clientHeight;
                var windowHeight = window.innerHeight;

                if (docHeight <= windowHeight) {
                  bootLoad();
                }
              });
            }, function(errorMsg) {
              //if we had some movies in array, it would meant that we got all data
              //and tried to fetch nonexistent page
              //don't show this error to user, show error only if movie array is empty
              if (msc.movies.length == 0) {
                curPage = 0;
                msc.errorMsg = errorMsg;
              }
            });
        })();
      }

      function paginationDataAdder() {
        curPage++;

        return FilteredMovieSet.get({
            s: msc.nameFilter,
            y: parseInt(msc.yearFilter) || '',
            type: msc.typeFilter,
            page: curPage
          })
          .then(function(movies) {
            msc.errorMsg = '';
            msc.movies = msc.movies.concat(movies);
          }, function(errorMsg) {
            if (msc.movies.length == 0) {
              curPage = 0;
              msc.errorMsg = errorMsg;
            }
          });
      }

      function emptyCache() {
        msc.hasCache = false;
        FilteredMovieSet.emptyCache();
      }

    }]);
})();
