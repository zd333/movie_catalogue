;
(function() {
  'use strict';

  angular
    .module('movieCatalogue.controllers')
    .controller('MovieDetailsController', ['$routeParams', 'Movie', function($routeParams, Movie) {
      var mdc = this;

      mdc.movie = null;
      mdc.errorMsg = '';

      Movie.get({i: $routeParams.imdbID}) //TODO: would be good to incapsulate back-end parameter details in service and use simple id here
        .then(function(movie) {
          mdc.errorMsg = '';
          mdc.movie = movie;
        }, function(errorMsg) {
          mdc.movie = null;
          mdc.errorMsg = errorMsg;
        });
    }]);
})();
